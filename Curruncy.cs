﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curruncy : MonoBehaviour
{
    public float Speed;

    void Update()
    {
        transform.Translate(-Vector3.right * Time.deltaTime * Speed, Space.Self);
    }
}
