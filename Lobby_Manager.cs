﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


public class Lobby_Manager : MonoBehaviour
{
    public List<GameObject> UIs;
    public List<GameObject> SettingElemnts;
    public TextMeshProUGUI[] AllAchivmentsText;
    public Sprite[] LanguageSptites;
    public Sprite[] PlayButtonSprites;
    public Sprite[] BestOfTheBest_Sprites;
    public int LanguageIndex;
    public bool isVibration, isMusic, isSound;
    public GameObject PlayerButton, BestOfTheBest;
    public AudioSource BackGroundAudio, ClickAudio;

    public List<int> CoinsList ;
    private void Start() {
        SettingElemnts[3].GetComponent<Image>().sprite = LanguageSptites[PlayerPrefs.GetInt("LanguageIndex")];
        PlayerButton.GetComponent<Image>().sprite = PlayButtonSprites[PlayerPrefs.GetInt("LanguageIndex")];
        BestOfTheBest.GetComponent<Image>().sprite = BestOfTheBest_Sprites[PlayerPrefs.GetInt("LanguageIndex")];
        isMusic = PlayerPrefs.GetInt("Music") == 0 ? false : true;
        isSound = PlayerPrefs.GetInt("Sound") == 0 ? false : true;
        isVibration = PlayerPrefs.GetInt("Vibratation") == 0 ? false : true;
        
        SettingElemnts[0].SetActive(isVibration);
        SettingElemnts[1].SetActive(isSound);
        SettingElemnts[2].SetActive(isMusic);

        BackGroundAudio.enabled = isMusic;
    }
    public void Play(){
        ClickCound();
        SceneManager.LoadScene(1);
    }
    public void MainMenu(){
        ClickCound();
        SetUI(0);
    }
    public void Achimnets(){
        ClickCound();
        SetUI(1);
        AlignValues();
        AchimentCalled();
    }
    public void Setting(){
        ClickCound();
        SetUI(2);
    }
    public void DeletAllRecords(){

        PlayerPrefs.SetInt("FirstMain", 0);
        PlayerPrefs.SetInt("SecondMain", 0);
        PlayerPrefs.SetInt("ThirdMain", 0);
        PlayerPrefs.SetInt("LastMain",0);
        AlignValues();
        AchimentCalled();
    }
    public void Music(){
        ClickCound();
        isMusic = !isMusic;
        PlayerPrefs.SetInt("Music", isMusic ? 1 : 0);
        BackGroundAudio.enabled = PlayerPrefs.GetInt("Music") == 1;
        SettingElemnts[2].SetActive(isMusic);
    }
    public void Vibration(){
        ClickCound();
        isVibration = !isVibration;
        PlayerPrefs.SetInt("Vibratation", isVibration ? 1 : 0);
        if(isVibration)Handheld.Vibrate();
        SettingElemnts[0].SetActive(isVibration);
    }
    public void Sound(){
        ClickCound();
        isSound = !isSound;
        PlayerPrefs.SetInt("Sound", isSound ? 1 : 0);
        SettingElemnts[1].SetActive(isSound);
    }
    public void Language(){
        ClickCound();
        int x = LanguageIndex;
        x += 1;
        if(x > 2) x = 0;
        LanguageIndex = x;
        PlayerPrefs.SetInt("LanguageIndex", LanguageIndex);
        SettingElemnts[3].GetComponent<Image>().sprite = LanguageSptites[LanguageIndex];
        PlayerButton.GetComponent<Image>().sprite = PlayButtonSprites[LanguageIndex];
        BestOfTheBest.GetComponent<Image>().sprite = BestOfTheBest_Sprites[PlayerPrefs.GetInt("LanguageIndex")];
    }
    public void SetUI(int x){
        foreach (var item in UIs)
        {
            item.SetActive(false);
        }
        UIs[x].SetActive(true);
    }
    public void AlignValues(){
        CoinsList = new List<int>(4);
        CoinsList.Add(PlayerPrefs.GetInt("FirstMain"));
        Debug.Log(PlayerPrefs.GetInt("FirstMain"));
        CoinsList.Add(PlayerPrefs.GetInt("SecondMain"));
        CoinsList.Add(PlayerPrefs.GetInt("ThirdMain"));
        CoinsList.Add(PlayerPrefs.GetInt("LastMain"));

        for (int i = 0; i < CoinsList.Count-1; i++)
        {
            for (int j = 0; j < CoinsList.Count-i-1; j++)
            {
                if(CoinsList[j] < CoinsList[j+1]){
                    int k = CoinsList[j];
                    CoinsList[j] = CoinsList[j+1];
                    CoinsList[j+1] = k;
                }
            }
        }

        PlayerPrefs.SetInt("FirstMain", CoinsList[0]);
        PlayerPrefs.SetInt("SecondMain",  CoinsList[1]);
        PlayerPrefs.SetInt("ThirdMain",  CoinsList[2]);

        CoinsList.Clear();
    } 
    public void AchimentCalled(){
        AllAchivmentsText[0].text = "" + PlayerPrefs.GetInt("FirstMain");
        AllAchivmentsText[1].text = "" + PlayerPrefs.GetInt("SecondMain");
        AllAchivmentsText[2].text = "" + PlayerPrefs.GetInt("ThirdMain");
    }   
    public void ClickCound(){
        if(isSound){
        ClickAudio.enabled = false;
        ClickAudio.enabled = true;
        }
    }
}
