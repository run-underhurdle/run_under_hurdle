﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public GameObject Main;
    Vector3 position;
    public Vector3 EndVector;
    public float LastDownposition, LastUpposition;
    public bool isReachedToTop;
    
    private void Start() {
        isReachedToTop = Random.Range(0,1) == 1 ? true : false;
        position = new Vector3(0, Random.Range(0, LastDownposition), 0);
        Debug.Log(position);
    }

    private void Update() {
        if(isReachedToTop) position.y -= Time.deltaTime;
        else position.y += Time.deltaTime;

        if(position.y < LastDownposition) 
        {
            position.y = LastDownposition;
            isReachedToTop = false;
        }else if(position.y > LastUpposition)
        {
            position.y = LastUpposition;
            isReachedToTop = true;
        }

        Main.transform.localPosition = position;

        transform.Translate(-Vector3.right * Time.deltaTime, Space.Self);
        
        if(transform.position.x < EndVector.x) Destroy(gameObject);
    }
}
