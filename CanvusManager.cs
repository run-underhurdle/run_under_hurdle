﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class CanvusManager : MonoBehaviour
{
    public GameObject GameOverPanel, Player;
    public AudioSource BackgroundAudio, ClickAudio, LoseAudio;
    public Image RetstartIm, MainMenuIm;
    public TextMeshProUGUI ContinueText;
    public Sprite[] RestartButton, MainMenuButton;
    public string[] ContinueInLang;
    private void Start() {
        StartBackGroundSound();
    }
    public void Retart()
    {
        ClickCound();
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }
    public void MainMenu()
    {
        ClickCound();
        PlayerPrefs.SetInt("LastMain", Player.GetComponent<Player>().points);
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void Continue(){
        ClickCound();StartBackGroundSound();
        if(Player.GetComponent<Player>().points >= 50){
            GameOverPanel.SetActive(false);
            Time.timeScale = 1;
            Player.GetComponent<Player>().Continue();
        }
    }
    public void GameOver(){
        BackgroundAudio.enabled = false;
        LoseSound();
        GameOverPanel.SetActive(true);
        OpenGameOverPanel();
        
    }
    public void StartBackGroundSound(){
        if(PlayerPrefs.GetInt("Music") == 1){
        BackgroundAudio.enabled = true;
        }
    }
    public void ClickCound(){
        if(PlayerPrefs.GetInt("Sound") == 1){
        ClickAudio.enabled = false;
        ClickAudio.enabled = true;
        }
    }
    public void LoseSound(){
        if(PlayerPrefs.GetInt("Sound") == 1){
        LoseAudio.enabled = false;
        LoseAudio.enabled = true;
        }
    }
    public void OpenGameOverPanel(){
        RetstartIm.sprite = RestartButton[PlayerPrefs.GetInt("LanguageIndex")];
        MainMenuIm.sprite = MainMenuButton[PlayerPrefs.GetInt("LanguageIndex")];
        ContinueText.text = "" + ContinueInLang[PlayerPrefs.GetInt("LanguageIndex")] + 50 + " )" ;
    }
}
