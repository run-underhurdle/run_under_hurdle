﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public GameObject Obstacle;
    public GameObject List;
    public int TimeToSpawnMax, TimeToSpawnMin;
    public float TimeToSpawn;
    public float SpawnTimer;

    public Transform StartPosition, EndPosition;
    private void Start() {
        SpawnTimer = TimeToSpawn;
    }
    private void Update()
    {
        if(Time.time > SpawnTimer + TimeToSpawn)
        {
            SpawnTimer = Time.time;
            TimeToSpawn = Random.Range(TimeToSpawnMin, TimeToSpawnMax);
            GameObject obstacle = Instantiate(Obstacle, StartPosition.position, Quaternion.identity, List.transform);
            if(obstacle.GetComponent<Obstacle>() != null)obstacle.GetComponent<Obstacle>().EndVector = EndPosition.position;
        }
    }
}
