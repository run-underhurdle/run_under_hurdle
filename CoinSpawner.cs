﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public GameObject[] Coins;
    public float TimeToSpawn;
    public GameObject CoinsList;
    public float SpawnTimer;

    public Transform StartPosition, EndPosition;
    private void Start() {
        SpawnTimer = TimeToSpawn;
    }
    private void Update()
    {
        if(Time.time > SpawnTimer + TimeToSpawn)
        {
            SpawnTimer = Time.time;
            GameObject obstacle = Instantiate(Coins[Random.Range(0,Coins.Length)], StartPosition.position, Quaternion.identity, CoinsList.transform);
            if(obstacle.GetComponent<Obstacle>() != null)obstacle.GetComponent<Obstacle>().EndVector = EndPosition.position;
        }
    }
}
