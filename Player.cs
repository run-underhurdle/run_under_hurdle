﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public float MaxMovementOnX, MovementOnX, TransitionSpeed;
    public GameObject[] Lifes;
    public TextMeshProUGUI poinstText;
    public Animator Animator;
    public Vector3 Playerposition;
    public int LifesGone;
    public bool isRunning;
    Touch touch;
    public int points;
    
    public GameObject CanvusManager;
    private void Start() {
        LifesGone = 0;
        points = 0;
        poinstText.text = ""+ 000000;
        Playerposition = transform.position;
        
    }

    private void Update()
    {
        // Input System
        isRunning = Input.GetKey(KeyCode.Mouse0) ? true : Input.GetKey(KeyCode.Space);

        if(isRunning){
            if(MovementOnX >= MaxMovementOnX)MovementOnX = MaxMovementOnX;
            else MovementOnX += Time.deltaTime * TransitionSpeed; 
        }else{
            if(MovementOnX <= 0)MovementOnX = 0;
            else MovementOnX -= Time.deltaTime * TransitionSpeed; 
        }
        transform.position = Playerposition + new Vector3(MovementOnX, 0);

        //Animator
        Animator.SetBool("Run", isRunning);


        // Lifes
        int x = 5-LifesGone;
        if(LifesGone != 0 && LifesGone <=5) Lifes[x].SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Obstacle")){
            LifesGone++;
            other.GetComponent<Collider2D>().enabled = false;
            if(LifesGone >= 5){
                Debug.Log("Dead");
                Death();
                Handheld.Vibrate();
            }
        }
        if(other.CompareTag("Crystal")){
            Debug.Log("Crystal");
            points += 20;
            Handheld.Vibrate();
            AsignPoints();
            Destroy(other.gameObject);
        }
        if(other.CompareTag("Coin")){
            Debug.Log("Coin");
            points += 10;
            Handheld.Vibrate();
            AsignPoints();
            Destroy(other.gameObject);
        }
    }
    public void Death(){
        Time.timeScale = 0;
        CanvusManager.GetComponent<CanvusManager>().GameOver();
    }
    public void Continue(){
        points -= 50; 
        AsignPoints();
        LifesGone = 0;
        foreach (var item in Lifes)
        {
            item.SetActive(true);
        }
    }
    public void AsignPoints(){
        poinstText.text = "" + points;
    }
}
